# Forecast Server API documentation

## Table of Contents
1. [Overview](#overview)
2. [Information requests](#information-requests)
3. [Data requests](#data-requests)

## Overview
----
  The API has been divided into two parts. The first part contains methods for getting information about the forecasts while the other part contains
  methods for accessing the forecast data itself.
  
  A general note is that missing or unexpected query string or data parameters will currently return a **404 Not Found** HTML error page without any CORS headers set.
  This is currently handled by the web framework. At a later stage we will however change this so that missing parameter error responses will be in JSON and unexpected
  query string parameters are ignored (since they are often added to avoid upstream caching).

## Information requests
----
**Parameters information**
----
  This method will fetch a list containing the names of available forecast variables.

* **URL**

  /info/parameters

* **Method:**
  
  `GET`

* **URL Params**

  None 

* **Success Response:**
  
  Example:

  * **Code:** 200 OK <br />
    **Content:** `["Wind", "WindSpeed", "Precipitation", "CloudBaseHeight", "SpecificHumidity", "TotalCloudCover", "SeaLevelPressure", "AirTemperature", "Visibility", "WavePeriod", "WaveHeight", "WaveHeight2D", "SeaSurfaceSalinity", "SeaSurfaceTemperature", "SeaSurfaceCurrent", "SeaSurfaceCurrentSpeed", "Sealevel"]`
 
* **Error Response:**

  * **Code:** 500 Internal Server Error <br />
    **Content:** `{message: "<reason for server error>"}`

* **Sample Call:**

  * https://api.fcoo.dk/metoc/v2/info/parameters

**Timestep information**
----
  This method will fetch a list of available timesteps for one forecast variable.
  A single variable can draw on multiple forecasts depending on the area
  a forecast is requested for. It is therefore also possible to provide a
  position. The fetched timesteps will then be the timesteps available at the
  specific position.

* **URL**

  /info/timesteps

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `variable=[alphanumeric]`

   **Optional:**

   `lat=[float]`

   `lon=[float]`


* **Success Response:**
  
  * **Code:** 200 OK <br />
    **Content:** `['2016-12-04T12:00:00', '2016-12-04T15:00:00',..., '2016-12-06T12:00:00']`
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** `{message: "Variable <variable> not found"}` <br />
    **Content:** `{message: "Invalid 'lat'|'lon' parameter"}`

  * **Code:** 500 Internal Server Error <br />
    **Content:** `{message: "<reason for server error>"}`

* **Sample Call:**

  * https://api.fcoo.dk/metoc/v2/info/timesteps?variable=Precipitation

**Area information**
----
  This method will fetch a bounding box for the forecasts for one variable.

* **URL**

  /info/area

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `variable=[alphanumeric]`

* **Success Response:**
  
  * **Code:** 200 OK <br />
    **Content:** `{"lon_min": -11, "lat_max": 66.2, "lat_min": 49, "lon_max": 26.41517}`
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** `{message: "Variable <variable> not found"}`

  * **Code:** 500 Internal Server Error <br />
    **Content:** `{message: "<reason for server error>"}`

* **Sample Call:**

  * https://api.fcoo.dk/metoc/v2/info/area?variable=Precipitation

## Data requests
----
**Timeseries forecast data**
----
  This method will fetch a dictionary of timeseries forecasts for multiple
  variables at one specific position.

* **URL**

  /data/timeseries

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `variable=[alphanumeric]`

   `lat=[float]`

   `lon=[float]`

* **Success Response:**
  
  * **Code:** 200 OK <br />
    **Content:** `{"SeaSurfaceCurrent": {"uu": {"lat": 55.991667, "attributes": {"units": "knots", "long_name": "zonal vel.", "missing_value": -9999}, "lon": 4.4027778, "data": [0.33001843, 0.33618218, 0.35879022, 0.39968237, 0.44267559, 0.49351665, 0.54221112, 0.5489282, 0.54123962, 0.47974798, 0.48574004, 0.48525837, 0.51549971, 0.55277157, 0.56736207, 0.54712892, 0.48597646, 0.39298362, 0.28238678, 0.1626467, 0.039741643, -0.034215298, -0.085047953, -0.052623175, -0.032980755, 0.0022686473, 0.074616559, 0.083245613, 0.03985576, -0.015248271, -0.091029339, -0.21319744, -0.33166844, -0.40682265, -0.4749679, -0.46015987, -0.34027457, -0.17172502, 0.022439364, 0.19998974, 0.26929677, 0.25171027, 0.21233673, 0.13275568, 0.024438359, -0.1234037, -0.21524529, -0.2578792, -0.30347693, -0.21821049, -0.081003048, 0.077363819, 0.21532902, 0.24067596, 0.1816026, 0.093357988], "time": ["2016-12-07T05:00:00Z", "2016-12-07T06:00:00Z", "2016-12-07T07:00:00Z", "2016-12-07T08:00:00Z", "2016-12-07T09:00:00Z", "2016-12-07T10:00:00Z", "2016-12-07T11:00:00Z", "2016-12-07T12:00:00Z", "2016-12-07T13:00:00Z", "2016-12-07T14:00:00Z", "2016-12-07T15:00:00Z", "2016-12-07T16:00:00Z", "2016-12-07T17:00:00Z", "2016-12-07T18:00:00Z", "2016-12-07T19:00:00Z", "2016-12-07T20:00:00Z", "2016-12-07T21:00:00Z", "2016-12-07T22:00:00Z", "2016-12-07T23:00:00Z", "2016-12-08T00:00:00Z", "2016-12-08T01:00:00Z", "2016-12-08T02:00:00Z", "2016-12-08T03:00:00Z", "2016-12-08T04:00:00Z", "2016-12-08T05:00:00Z", "2016-12-08T06:00:00Z", "2016-12-08T07:00:00Z", "2016-12-08T08:00:00Z", "2016-12-08T09:00:00Z", "2016-12-08T10:00:00Z", "2016-12-08T11:00:00Z", "2016-12-08T12:00:00Z", "2016-12-08T13:00:00Z", "2016-12-08T14:00:00Z", "2016-12-08T15:00:00Z", "2016-12-08T16:00:00Z", "2016-12-08T17:00:00Z", "2016-12-08T18:00:00Z", "2016-12-08T19:00:00Z", "2016-12-08T20:00:00Z", "2016-12-08T21:00:00Z", "2016-12-08T22:00:00Z", "2016-12-08T23:00:00Z", "2016-12-09T00:00:00Z", "2016-12-09T01:00:00Z", "2016-12-09T02:00:00Z", "2016-12-09T03:00:00Z", "2016-12-09T04:00:00Z", "2016-12-09T05:00:00Z", "2016-12-09T06:00:00Z", "2016-12-09T07:00:00Z", "2016-12-09T08:00:00Z", "2016-12-09T09:00:00Z", "2016-12-09T10:00:00Z", "2016-12-09T11:00:00Z", "2016-12-09T12:00:00Z"]}, "vv": {"lat": 55.991667, "attributes": {"units": "knots", "long_name": "meridional vel.", "missing_value": -9999}, "lon": 4.4027778, "data": [0.47553006, 0.58663416, 0.71177506, 0.81120539, 0.86392516, 0.8742671, 0.81574696, 0.70847732, 0.58998185, 0.49926111, 0.45876223, 0.42786875, 0.43991604, 0.45565328, 0.4613075, 0.43741485, 0.34332925, 0.21000829, 0.064885512, -0.1026449, -0.24158813, -0.34736541, -0.40156341, -0.41905016, -0.39765781, -0.31473294, -0.25504592, -0.22832425, -0.24590239, -0.29406938, -0.36786306, -0.47920516, -0.56324041, -0.60731792, -0.57145292, -0.42140737, -0.2574721, -0.13231151, -0.011335934, 0.073680215, 0.095426999, 0.088894561, 0.011176762, -0.12498022, -0.30779189, -0.4873046, -0.59757733, -0.60631245, -0.52525562, -0.40363815, -0.27393273, -0.18475401, -0.14461109, -0.1437221, -0.15765902, -0.17723139], "time": ["2016-12-07T05:00:00Z", "2016-12-07T06:00:00Z", "2016-12-07T07:00:00Z", "2016-12-07T08:00:00Z", "2016-12-07T09:00:00Z", "2016-12-07T10:00:00Z", "2016-12-07T11:00:00Z", "2016-12-07T12:00:00Z", "2016-12-07T13:00:00Z", "2016-12-07T14:00:00Z", "2016-12-07T15:00:00Z", "2016-12-07T16:00:00Z", "2016-12-07T17:00:00Z", "2016-12-07T18:00:00Z", "2016-12-07T19:00:00Z", "2016-12-07T20:00:00Z", "2016-12-07T21:00:00Z", "2016-12-07T22:00:00Z", "2016-12-07T23:00:00Z", "2016-12-08T00:00:00Z", "2016-12-08T01:00:00Z", "2016-12-08T02:00:00Z", "2016-12-08T03:00:00Z", "2016-12-08T04:00:00Z", "2016-12-08T05:00:00Z", "2016-12-08T06:00:00Z", "2016-12-08T07:00:00Z", "2016-12-08T08:00:00Z", "2016-12-08T09:00:00Z", "2016-12-08T10:00:00Z", "2016-12-08T11:00:00Z", "2016-12-08T12:00:00Z", "2016-12-08T13:00:00Z", "2016-12-08T14:00:00Z", "2016-12-08T15:00:00Z", "2016-12-08T16:00:00Z", "2016-12-08T17:00:00Z", "2016-12-08T18:00:00Z", "2016-12-08T19:00:00Z", "2016-12-08T20:00:00Z", "2016-12-08T21:00:00Z", "2016-12-08T22:00:00Z", "2016-12-08T23:00:00Z", "2016-12-09T00:00:00Z", "2016-12-09T01:00:00Z", "2016-12-09T02:00:00Z", "2016-12-09T03:00:00Z", "2016-12-09T04:00:00Z", "2016-12-09T05:00:00Z", "2016-12-09T06:00:00Z", "2016-12-09T07:00:00Z", "2016-12-09T08:00:00Z", "2016-12-09T09:00:00Z", "2016-12-09T10:00:00Z", "2016-12-09T11:00:00Z", "2016-12-09T12:00:00Z"]}}}`
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** `{message: "Variable <variable> not found"}` <br />
    **Content:** `{message: "Invalid 'lat'|'lon' parameter"}`

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{message: "No forecast at requested position"}`

  * **Code:** 500 Internal Server Error <br />
    **Content:** `{message: "<reason for server error>"}`

* **Sample Call:**

  * https://api.fcoo.dk/metoc/v2/data/timeseries?variables=SeaSurfaceCurrent&lat=56.0&lon=11.0


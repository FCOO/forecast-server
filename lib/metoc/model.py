# encoding: utf-8
"""
NetCDF model.
"""
# Standard library imports
from urllib.request import urlopen
import datetime

# External imports
import numpy as np
import netCDF4
#from pydap.client import open_url

# Local imports
from . import forecast_exceptions

class Dataset:
    def __init__(self, files, variable_names):
        self.files = files
        self.dataset = netCDF4.MFDataset(files, 'r')
        self.variable_names = variable_names
        for varname in self.variable_names:
            if varname not in self.dataset.variables:
                msg = 'Invalid variable name %s' % varname
                raise ValueError(msg)

    def _get_latlons(self, varname):
        var = self.dataset.variables[varname]
        attrs = var.__dict__
        if 'coordinates' in attrs:
            coords = attrs['coordinates'].split()
            lons = self.dataset.variables[coords[0]]
            lats = self.dataset.variables[coords[1]]
        else:
            dims = var.dimensions
            lats = self.dataset.variables[dims[1]]
            lons = self.dataset.variables[dims[2]]
        return lats, lons

    def timesteps(self, varnames):
        """\
        Returns timesteps for variables. The variables must have
        identical time dimensions.
        """
        # Check whether time dimensions are identical
        tdims = []
        for varname in varnames:
            var = self.dataset.variables[varname]
            dims = var.dimensions
            tdim = dims[0]
            if tdim not in tdims:
                tdims.append(dims[0])
        if len(tdims) != 1:
            msg = 'Variables %s must have same time dimensions' % (varnames)
            raise ValueError(msg)
        
        # Extract information for first variable
        varname = varnames[0]
        var = self.dataset.variables[varname]
        attrs = var.__dict__
        dims = var.dimensions
        # Construct ISO-8601 formatted time list
        t = self.dataset.variables[dims[0]]
        units = t.units
        calendar = t.__dict__.get('calendar', 'standard')
        tdate = netCDF4.num2date(t[:], units, calendar=calendar)
        fmt_out = '%Y-%m-%dT%H:%M:%SZ'
        tstr = [tm.strftime(fmt_out) for tm in tdate]
        
        return tstr

    #@profile
    def timeseries(self, lat, lon):
        """Returns timeseries data at specific position."""
        output = {}
        for varname in self.variable_names:
            var = self.dataset.variables[varname]
            long_name = var.long_name
            units = var.units
            attrs = var.__dict__
            dims = var.dimensions
            if len(dims) != 3:
                raise ValueError('No timeseries support for non 3D fields')

            # Construct ISO-8601 formatted time list
            t = self.dataset.variables[dims[0]]
            tunits = t.units
            calendar = t.__dict__.get('calendar', 'standard')
            tarray = t[:]
            tdate = netCDF4.num2date(tarray, tunits, calendar=calendar)
            fmt_out = '%Y-%m-%dT%H:%M:%SZ'
            tstr = [t.strftime(fmt_out) for t in tdate]
            #time = [int(unix_time(t)) for t in time]

            # Extract data
            lats, lons = self._get_latlons(varname)
            ilat, ilon = _latlon_to_indices(lats, lons, lat, lon)
            data = var[:,ilat,ilon]
            data = data.flatten().tolist()
            if len(lats.shape) > 1:
                alat = lats[ilat, ilon]
                alon = lons[ilat, ilon]
            else:
                alat = lats[ilat]
                alon = lons[ilon]
            alat = alat.flatten().tolist()[0]
            alon = alon.flatten().tolist()[0]

            # Put results into output dict
            output[varname] = {}
            output[varname]['attributes'] = {}
            output[varname]['attributes']['long_name'] = long_name
            output[varname]['attributes']['units'] = units
            if 'missing_value' in attrs:
                output[varname]['attributes']['missing_value'] = \
                        np.asscalar(attrs['missing_value'])
            output[varname]['data'] = data
            output[varname]['lat'] = alat
            output[varname]['lon'] = alon
            output[varname]['time'] = tstr
        return output

    def timeseries_area(self, lat1, lon1, lat2, lon2):
        """Returns timeseries data for a given area (lat1,lon1,lat2,lon2)."""
        output = {}
        for varname in self.variable_names:
            var = self.dataset.variables[varname]
            long_name = var.long_name
            units = var.units
            dims = var.dimensions
            if len(dims) != 3:
                raise ValueError('No timeseries support for non 3D fields')

            # Construct ISO-8601 formatted time list
            time = self.dataset.variables[dims[0]]
            tunits = time.units
            calendar = time.__dict__.get('calendar', 'standard')
            nctime = netcdftime.utime(tunits, calendar=calendar)
            time = nctime.num2date(time[:])
            fmt_out = '%Y-%m-%dT%H:%M:%SZ'
            time = [t.strftime(fmt_out) for t in time]
            #time = [int(unix_time(t)) for t in time]

            # Extract data
            lats, lons = self._get_latlons(varname)
            ilat1, ilon1 = _latlon_to_indices(lats, lons, lat1, lon1)
            ilat2, ilon2 = _latlon_to_indices(lats, lons, lat2, lon2)
            data = var[:,ilat1:ilat2+1,ilon1:ilon2+1].tolist()
            #data = var[:,ilat,ilon].flatten().tolist()

            # Put results into output dict
            output[varname] = {}
            output[varname]['attributes'] = {}
            output[varname]['attributes']['long_name'] = long_name
            output[varname]['attributes']['units'] = units
            output[varname]['data'] = data
            output[varname]['time'] = time
        return output

    #@profile
    def profile(self, lat, lon):
        """Returns profile data at specific position."""
        output = {}
        for varname in self.variable_names:
            var = self.dataset.variables[varname]
            attrs = var.__dict__
            dims = var.dimensions
            if len(dims) != 4:
                raise ValueError('No profile support for non 4D fields')

            # Construct ISO-8601 formatted time list
            time = self.dataset.variables[dims[0]]
            units = time.units
            calendar = time.__dict__.get('calendar', 'standard')
            nctime = netcdftime.utime(units, calendar=calendar)
            time = nctime.num2date(time[:])
            fmt_out = '%Y-%m-%dT%H:%M:%SZ'
            time = [t.strftime(fmt_out) for t in time]
            #time = [int(unix_time(t)) for t in time]

            # TODO: Find time index
            itime = 0

            # TODO: Find depth array
            #depths = 

            # Extract data
            lats, lons = self._get_latlons(varname)
            ilat, ilon = _latlon_to_indices(lats, lons, lat, lon)
            data = var[itime,:,ilat,ilon]
            data = data.flatten().tolist()
            if len(lats.shape) > 1:
                alat = lats[ilat, ilon]
                alon = lons[ilat, ilon]
            else:
                alat = lats[ilat]
                alon = lons[ilon]
            alat = alat.flatten().tolist()[0]
            alon = alon.flatten().tolist()[0]

            # Put results into output dict
            output[varname] = {}
            output[varname]['attributes'] = {}
            output[varname]['attributes']['long_name'] = attrs['long_name']
            output[varname]['attributes']['units'] = attrs['units']
            if 'missing_value' in attrs:
                output[varname]['attributes']['missing_value'] = \
                        np.asscalar(attrs['missing_value'])
            output[varname]['data'] = data
            output[varname]['lat'] = alat
            output[varname]['lon'] = alon
            output[varname]['depth'] = depths
            output[varname]['time'] = time[itime]
        return output

#@profile
def _latlon_to_indices(lats, lons, lat, lon):
    """Helper method for finding indices nearest a specific position."""
    if len(lats.shape) == 2:
        dist = np.sqrt((lats[:]-lat)**2 + (lons[:]-lon)**2)
        flat_idx = np.argmin(dist)
        ilat, ilon = np.unravel_index(flat_idx, dist.shape)
    else:
        ilat = np.argmin(np.abs(lats[:]-lat))
        ilon = np.argmin(np.abs(lons[:]-lon))
    return ilat, ilon

def unix_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

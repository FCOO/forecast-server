"""
Exceptions for the forecast server
"""
class Invalid(Exception):
    """Raised when user input is invalid."""
    pass

class NetCDFException(Exception):
    """Raised when a NetCDF error occurs."""
    pass

